package com.example.mad.trafficlights;

        import android.support.v4.content.ContextCompat;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.LinearLayout;

/**
 * Класс MainActivity предназначен для имитации работы светофора
 * @author Визирякин Павел группа 14ИТ18К
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button redButton;
    Button yellowButton;
    Button greenButton;
    private LinearLayout linearLayout;
    private static int background = R.color.white;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        redButton = (Button) findViewById(R.id.red_button);
        yellowButton = (Button) findViewById(R.id.yellow_button);
        greenButton = (Button) findViewById(R.id.green_button);
        redButton.setOnClickListener(this);
        yellowButton.setOnClickListener(this);
        greenButton.setOnClickListener(this);

    }

    /**
     * Анализируем, какая кнопка была нажата:
     * при нажатии кнопки {@code redButton} задний фон становится красным;
     * при нажатии кнопки {@code yellowButton} задний фон становится жёлтым;
     * при нажатии кнопки {@code greenButton} задний фон становится зелёным
     *
     * @param v - вьюшка
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.red_button:
                redButton.setText(R.string.redColor);
                linearLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.red));
                background = R.color.red;
                break;
            case R.id.yellow_button:
                yellowButton.setText(R.string.yellowColor);
                linearLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.yellow));
                background = R.color.yellow;
                break;
            case R.id.green_button:
                greenButton.setText(R.string.greenColor);
                linearLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
                background = R.color.green;
                break;
        }
    }


    /**
     * Сохраняет состояние Activity
     *
     * @param outState - временные данные в процессе работы Activity
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("background", background);

    }


    /**
     * Восстанавливает состояние приложения после изменений Activity
     *
     * @param savedInstanceState - данные для восстановления приложения
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("background")) {
            background = savedInstanceState.getInt("background");
            linearLayout.setBackgroundColor(ContextCompat.getColor(this, background));
        }

    }
}
